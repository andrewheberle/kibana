class kibana::config inherits kibana {
  if $kibana::config_manage {
    notify { 'Config management is not yet implemented.': }
  }
}