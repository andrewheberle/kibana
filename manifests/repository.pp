class kibana::repository inherits kibana {
  if $kibana::repository_manage {
    case $kibana::repository_type {
      'yum': {
        yumrepo { 'kibana':
          ensure   => $kibana::repository_ensure,
          name     => "kibana-${kibana::repository_version}",
          descr    => "kibana repository for ${kibana::repository_version} packages",
          baseurl  => "http://packages.elastic.co/kibana/${kibana::repository_version}/centos",
          gpgcheck => true,
          gpgkey   => $kibana::repository_key_source,
          enabled  => true,
        }
      }
      'apt': {
        include apt
        apt::source { 'kibana':
          comment  => "kibana repository for ${kibana::repository_version} packages",
          location => "http://packages.elastic.co/kibana/${kibana::repository_version}/debian",
          release  => 'stable',
          repos    => 'main',
          key      => {
            'id'     => '46095ACC8548582C1A2699A9D27D666CD88E42B4',
            'server' => 'pgp.mit.edu',
            'source' => $kibana::repository_key_source,
          },
          include  => {
            'deb' => true,
            'src' => false,
          },
        }
      }
      default: {
        fail("Module ${module_name} is only supported with apt or yum based distros")
      }
    }
  }
}