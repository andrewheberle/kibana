class kibana::install inherits kibana {
  if $kibana::package_manage {
    if $kibana::java_package_manage {
      package { $kibana::java_package_name:
        ensure => $kibana::java_package_ensure,
        before => Package[$kibana::package_name],
      }
    }

    package { $kibana::package_name:
      ensure => $kibana::package_ensure,
    }
  }
}