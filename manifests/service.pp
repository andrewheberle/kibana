class kibana::service inherits kibana {
  if $kibana::service_manage {
    service {$kibana::service_name:
      ensure => $kibana::service_ensure,
      enable => $kibana::service_enable,
    }
  }
}